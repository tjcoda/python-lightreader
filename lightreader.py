import cv2


def reverse_Bits(n, no_of_bits):
    result = 0
    for i in range(no_of_bits):
        result <<= 1
        result |= n & 1
        n >>= 1
    return result


# Binary Interpretation
flip = True
trimLeadingBit = False
endian = 'little'
reverseBits = True
bitsPerWord = 16

# Image Processing
outputPath = 'download.bin'
whiteDotX = 376
whiteDotY = 272
blackDotX = 364

luminanceThresholdForBit = 64

fps = 25
startFrameIndex = 60 * 11 * fps

frameCounter = 0
cap = cv2.VideoCapture("v25fps.mp4")
ret, frame = cap.read()
while(frameCounter < startFrameIndex):
    ret, frame = cap.read()
    cv2.imshow('frame', frame)
    frameCounter += 1
    if ret == False:
        break

frameCounter = 0

newFile = open(outputPath, 'wb')
newFileBytes = []

latestByte = 0
dataStarted = False

byteCount = 0

while(1):
    ret, frame = cap.read()
    if cv2.waitKey(1) & 0xFF == ord('q') or ret == False:
        cap.release()
        cv2.destroyAllWindows()
        break
    dataPixel = frame[whiteDotY, whiteDotX][0]
    binary = False if (dataPixel < luminanceThresholdForBit) else True
    if (flip):
        binary = not binary

    # Check for end of transmission
    backgroundPixel = frame[whiteDotY, blackDotX][0]
    if backgroundPixel > luminanceThresholdForBit:
        break

    if (dataStarted == False and binary != flip):
        dataStarted = True
        if trimLeadingBit:
            continue

    if dataStarted:
        latestByte = (latestByte << 1) | (binary & 0x1)

        if (frameCounter % bitsPerWord) == 0:
            if reverseBits:
                latestByte = reverse_Bits(latestByte, bitsPerWord)
            latestByte = latestByte & (0xffff)
            newFileBytes.append(latestByte)
            byteCount += 1
            print('Downloaded {} bytes - latest {}'.format(byteCount, latestByte))
            latestByte = 0

        frameCounter += 1

    cv2.imshow('frame', frame)

for byte in newFileBytes:
    newFile.write(byte.to_bytes(2, byteorder=endian, signed=False))
