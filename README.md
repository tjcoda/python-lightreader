Python Lightreader
============

Introduction
---

In 1985 a show on channel 4 called '4 Computer Bluffs' experimented with transmitting computer code via a blinking light on the screen.

The viewers would have previously built a device with a suction cup and a light sensor to attach to their (glass) TV screen to capture the signal for interpretation by their home computer.

Having stumbled upon this, I've been attempting to decipher the transmission.

Here is the broadcast which reports as 24.87 FPS (although the original broadcast would have been PAL 25FPS):

https://www.youtube.com/watch?v=OXFGwfbfBK0

The tranmission occurs about 10 minutes into the video clip.

Here is a previous episode in which (at the end) they broadcast audio streams for various home computers of the time to use as software to work with the physical device:

https://www.youtube.com/watch?v=WCuIrkvbyRI

Just five minutes...
---

Well, this turned out to be a longer task than five minutes. I've yet to successfully load the audio transmission into a BBC Micro emulator and don't know if I'm expecting the light transmission to look like BASIC or machine code.


Original Documentation
---

On pages 184-187 of PC World from March 1985 are the instructions for [building and programming the reciever](https://archive.org/stream/PersonalComputerWorld1985-03?ui=embed#page/184/mode/2up?ui=embed)

I've OCR'ed the BBC Micro listing, hand corrected the mistakes in OCR to produce .bas files.
Then I've written a quick script to copy the bytes out to a binary file.
Finally I've put the results through da65 (part of the cc65 tool set) to produce 6502 assembly files.